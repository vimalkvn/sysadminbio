A docker image for viga
=======================
This repository contains the Dockerfile for building a 
[Docker](https://www.docker.com) image for
[viga](https://github.com/EGTortuero/viga) -
*a de novo viral genome annotator*. If you have installed Docker,
you can pull this image using

	docker pull vimalkvn/viga

## Notes

1. The docker image built using this Dockerfile will have **viga**
   (version specified in the `Dockerfile`) from Github along
   with all the dependencies pre-installed - Aragorn, HMMER,
   Prodigal, BioPython, LASTZ, Tandem and Inverted repeats finder.
   
2. Ubuntu 14.04 is used as a base image.

3. To run this container, download the [run-viga](https://raw.githubusercontent.com/vimalkvn/sysadminbio/master/docker-images/viga/run-viga)
   wrapper script available from the Github repository, save it in a 
   location accessible in PATH and run it like this:
   
		run-viga \
		python /program/VIGA.py \
		--input rubella.fasta \
		--blastdb /data/databases/blast/nr/nr \
		--rfamdb /data/databases/rfam/Rfam.cm \
		--hmmdb /data/databases/UniProt/uniprot_trembl.fasta \
		--modifiers modifiers.txt \
		--threads 10
   
   or run the container directly like this:
	
		docker run --rm \
		-e LOGNAME=$(logname) \
		-e USER=$(logname) \
		-u ${UID}:${UID} \
		-v /data/databases:/data/databases:ro \
		-v $(pwd):/wdir \
		-w /wdir \
		vimalkvn/viga \
		python /program/VIGA.py \
		--input rubella.fasta \
		--blastdb /data/databases/blast/nr/nr \
		--rfamdb /data/databases/rfam/Rfam.cm \
		--hmmdb /data/databases/UniProt/uniprot_trembl.fasta \
		--modifiers modifiers.txt \
		--threads 10

   The above methods assume that databases are located in
   */data/databases*. If this is not the case, please change the
   wrapper script or the command accordingly. Please note the
   databases specified in ``--blastdb``, ``--rfamdb`` and
   ``--hmmdb`` should all be available inside the container. The
   simple approach would be to save all databases in one location and
   export the top level directory using the ``-v`` option.

   The ``-e`` and ``-u`` options are used so that the container has
   permissions to read input files and the generated output files will
   be owned by the user running the container and not root. Default
   user is *viga*.

   If you are using one of the methods above, input files are
   assumed to be present in the directory where the command is run.

   Other viga program options can be specified and are sent as is.

   User must have *either*:

   * sudo access to run this script (script should be run as
   ``sudo run-viga``) or the

   * user must be part of the *docker* user group.


