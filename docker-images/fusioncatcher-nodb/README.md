# README - fusioncatcher-nodb

This Dockerfile can be used to build 
[fusioncatcher](https://github.com/ndaniel/fusioncatcher)
without databases.

## Run fusioncatcher

To run fusioncatcher using this Docker image, use the command::

	docker run --rm vimalkvn/fusioncatcher-nodb fusioncatcher
	
To share a directory (example: `/data`), use::

	docker run --rm -v /data:/data vimalkvn/fusioncatcher-nodb fusioncatcher

To share the current directory too, use::

	docker run --rm -v /data:/data -v $(pwd):/workdir -w /workdir vimalkvn/fusioncatcher-nodb fusioncatcher
	
The container will then have access to all the files in the current
directory.

## Databases

To use databases with this image, save them under `/data` or 
another directory that you share with the container. 

Alternatively, you should be able to use `fusioncatcher-build` to build the
databases and indexes (not tested):

	docker run --rm -v /data:/data vimalkvn/fusioncatcher-nodb fusioncatcher-build

For any questions related to the program itself, please consult 
the [manual](https://github.com/ndaniel/fusioncatcher/blob/master/doc/manual.md).

