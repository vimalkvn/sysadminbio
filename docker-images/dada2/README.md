A docker image for dada2
========================
This repository contains the Dockerfile for building a
[Docker](https://www.docker.com) image for
[dada2](http://benjjneb.github.io/dada2/) ``1.4.0`` - an R package for 
"Fast, accurate, single-nucleotide resolution for amplicon data". 
You can pull this image using

	docker pull vimalkvn/dada2

## Notes

1. The docker image built using this Dockerfile will have the DADA2 
   1.4.0 from BioConductor.
   
2. R 3.4.0 is used as a base (``r-base:3.4.0``).

3. To run this container, download the [run-dada2](https://raw.githubusercontent.com/vimalkvn/sysadminbio/master/docker-images/dada2/run-dada2)
   wrapper script available from the Github repository, save it in a 
   location accessible in PATH, make it executable and run it like 
   this:
   
		run-dada2 script.R
   
   or run the container directly like this:
	
		docker run --rm \
		-e LOGNAME=$(logname) \
		-e USER=$(logname) \
		-u ${UID}:${UID} \
		-v $(pwd):/wdir \
		-w /wdir \
		vimalkvn/dada2 \
		script.R

   The ``-e`` and ``-u`` options are used so that the container has
   permissions to read input files and the generated output files will
   be owned by the user running the container and not root. 
   
   If you are using one of the methods above, input files are
   assumed to be present in the directory where the command is run.

   User must have *either*:

   * sudo access to run this script (script should be run as
   ``sudo run-dada2``) or the

   * user must be part of the *docker* user group.

4. This container simply runs ``Rscript`` by default and a script can
   be provided as the first argument. If a command other than
   ``Rscript`` is required, it can be specified using the 
   ``--entrypoint`` docker argument.

