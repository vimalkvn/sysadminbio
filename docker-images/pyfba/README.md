A docker image for PyFBA
========================
This repository contains the Dockerfile for building a
[Docker](https://www.docker.com) image for
[PyFBA](https://github.com/linsalrob/PyFBA) - A python implementation 
of flux balance analysis to model microbial metabolism. If you have
Docker installed, you can pull this image using:

	docker pull vimalkvn/pyfba

## Notes

1. The docker image built using this Dockerfile will have the latest
   version of PyFBA from Github along with all the dependencies
   pre-installed - GLPK, PyGLPK, libSBML, lxml, Beautiful Soup 4 and 
   the ModelSEED database.
   
2. To run a script using this container, download the [run-pyfba](https://raw.githubusercontent.com/vimalkvn/sysadminbio/master/docker-images/pyfba/run-pyfba)) 
   wrapper script available from the Github repository, save it in a 
   location accesible in PATH and run it like this:
   
		run-pyfba python your_fba_script.py
	   
   or run the docker container directly:

		docker run --rm \
		-e LOGNAME=$(logname) \
		-e USER=$(logname) \
		-u ${UID}:${UID} \
		-v $(pwd):/wdir \
		-w /wdir \
		vimalkvn/pyfba \
		python your_fba_script.py


3. The ModelSEED database is stored inside the container at 
   */data/ModelSEEDDatabase* and the PyFBA media directoy is copied to
   */data/media*. The following environmental variables are also set
   
		ModelSEEDDatabase="/data/ModelSEEDDatabase"
		PYFBA_MEDIA_DIR="/data/media"
   
   The ``-e`` and ``-u`` options are used so that the container has
   permissions to read input files and the generated output files will
   be owned by the user running the container and not root. Default
   user is *pyfba*.

   Input files are assumed to be present in the directory where 
   the command is run.

   User must have *either*:

   * sudo access to run this script (script should be run as
   ``sudo run-pyfba``) or the

   * user must be part of the *docker* user group.
