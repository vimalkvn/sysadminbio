A docker image for cutadapt
===========================
This repository contains the Dockerfile for building a
[Docker](https://www.docker.com) image for
[cutadapt](https://github.com/marcelm/cutadapt) -
a program for removing adapter sequences from sequencing reads. If you
have installed Docker, you can pull this image using

	docker pull vimalkvn/cutadapt

## Notes

1. This image has cutadapt version 1.13 installed.

2. The python 3.5-slim docker image is used as a base image.

3. To run this container, use the [run-cutadapt](https://github.com/vimalkvn/sysadminbio/tree/master/docker-images/cutadapt/run-cutadapt)
   wrapper script like this:

		./run-cutadapt \
		-a CTGTCTCTTATACACATCTGACGCTGCCGACGA \
		-o output.fastq \
		input.fastq

   or run the container directly like this:

		docker run --rm \
		-v $(pwd):/data \
		-w /data \
		-e LOGNAME=$(logname) \
		-e USER=$(logname) \
		-u ${UID}:${UID} \
		vimalkvn/cutadapt \
		-a CTGTCTCTTATACACATCTGACGCTGCCGACGA \
		-o output.fastq \
		input.fastq


   The ``-e`` and ``-u`` options are used so that the container has
   permissions to read input files and the generated output files will
   be owned by the user running the container and not root. Default
   user is *cutadapt*.

   If you are using one of the methods above, input files are
   assumed to be present in the directory where the command is run.

   Other cutadapt program options can be specified and are sent as is.

   User must have *either*:

   * sudo access to run this script (script should be run as
   ``sudo run-cutadapt``) or the

   * user must be part of the *docker* user group.


