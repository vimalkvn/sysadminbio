# README

This is the source repository for the docker image
[vimalkvn/qiime](https://hub.docker.com/r/vimalkvn/qiime/). 

This image has QIIME 1.9.1 installed.

Notes:

* Based on Ubuntu 14.04.
* R (latest stable version for Ubuntu 14.04 from CRAN) and
  packages - ape, biom, optparse, RColorBrewer, randomForest, vegan, DESeq2
  and metagenomeSeq.
* Python 2 and QIIME python dependencies (numpy, matplotlib).
* All other dependencies installed using [qiime-deploy](https://github.com/qiime/qiime-deploy).

Run this container

	docker run --rm vimalkvn/qiime /bin/bash -c \
	"source /opt/qiime_deps/activate.sh;print_qiime_config.py"

or use the included ``run-qiime`` wrapper script.

Todo

print_qiime_config.py -tf should pass with the exception of the test for finding
usearch. Possibly users can then save the usearch binary in the current
directory and it can be added to the ``PATH``.

Thanks

Graham Dumpleton - http://blog.dscpl.com.au/2015/12/unknown-user-when-running-docker.html
The run-qiime wrapper now uses ``LOGNAME`` and ``USER`` variables so that
files generated are now owned by the user running the script and not the
default user qiime with ``UID`` of 1000.

