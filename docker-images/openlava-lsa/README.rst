docker-openlava-lsa
===================

An `OpenLava <http://openlava.org>`_ docker image configured for the 
`LSA <https://github.com/brian-cleary/LatentStrainAnalysis>`_ package.

Ubuntu 14.04 is used as a base and OpenLava 3.2 is compiled from source. 

All dependencies needed by LSA are installed.

Usage
-----
1. Create the container ::

        docker run -it -h openlava-m1 -v /data:/data vimalkvn/docker-openlava-lsa /bin/bash

   The ``-h`` option is necessary to set the host name in ``/etc/hosts``. Without this,
   the openlava services do not start.

   ``/data`` is shared between the host and the container (``-v``).

2. Start openlava services ::

        service openlava start

   Check status using ``service openlava status``. A valid PID must be present for 
   all services.

3. Submit jobs as the ``openlava`` user ::

        su - openlava
        bsub 'sleep 10; echo `date` > date.txt'

4. For details on performing analysis with LSA, please check the documentation of that project
   at `http://latentstrainanalysis.readthedocs.org <http://latentstrainanalysis.readthedocs.org>`_.

