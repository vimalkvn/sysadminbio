#!/usr/bin/env python
# Copyright 2017 Vimalkumar Velayudhan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Script to download or update BLAST databases

For help, run:

    python blastdb_updater.py -h

Steps performed

1. Create directory with date under :file:`updates/db/`.
2. Download database using ``update_blastdb --passive db``
3. Verify archives and uncompress them
4. Remove downloaded archives and md5
5. Check if the current directory is being accessed and if 
   not, update symlink

"""
import os
import logging
import argparse
import subprocess
import logging.handlers
from glob import glob
from time import gmtime, strftime


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    group = parser.add_argument_group('blastdb_updater options')

    #TODO: if db is not in db's supported by update_blastdb, raise an error. Provide
    # list of databases available
    group.add_argument('-d', '--db',
                       help='Database to download - nr, nt, swissprot. '
                       'Any database supported by update_blastdb', required=True)
    group.add_argument('-p', '--path',
                       help='Path to download database to. Default is current directory',
                       default=os.getcwd())
    parser.add_argument('-D', '--debug', help='Write debugging output', action='store_true')

    args = parser.parse_args()
    base_dir = os.path.abspath(args.path)

    # create base directory if it is missing
    if not os.path.exists(base_dir):
        os.mkdir(base_dir)

    # setup logging
    log_dir = os.path.join(base_dir, 'log')
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    log = logging.getLogger('blastdb_updater')
    format = logging.Formatter('%(levelname)-7s | %(asctime)s | %(message)s',
                               datefmt='%Y-%m-%d %I:%M:%S %p') 
    if args.debug:
        level = logging.DEBUG
        # also print messages to console
    else:
        level = logging.INFO
    log.setLevel(level)

    file_handler = logging.handlers.RotatingFileHandler(
        os.path.join(log_dir, 'blastdb_updater.log'), maxBytes=104857, backupCount=2)
    file_handler.setFormatter(format)  
    log.addHandler(file_handler)
    
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(format)
    log.addHandler(console_handler)

    db = args.db
    date = strftime('%Y-%m-%d', gmtime())
    db_dir = os.path.join(base_dir, 'updates', db, date)
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)

    os.chdir(db_dir)
    log.info('Downloading "{}" database from NCBI FTP using update_blastdb'.format(db))
    ret_code =  subprocess.call(['update_blastdb', '--passive', db])

    if ret_code == 0:
        log.info('No data available for download for database {}'.format(db))
    elif ret_code == 2:
        log.error('Error while trying to download database using update_blastdb')
    elif ret_code == 1:
        # return code of update_blastdb is 1 on successful operations
        # that downloaded files
        log.info('Database download complete')
        md5_files = glob('*.md5')
        for md5 in md5_files:
            tar_name = md5.rstrip('.md5')
            log.debug('Verifying {}'.format(tar_name))
            subprocess.check_call(['md5sum', '-c', md5])

            # uncompress
            log.debug('Uncompressing {}'.format(tar_name))
            subprocess.check_call(['tar', '-zxf', tar_name])

            # remove archive and associated md5
            os.remove(md5)
            os.remove(tar_name)

        # update symlink
        os.chdir(base_dir)
        log.debug('Changed to base directory {}'.format(base_dir))
        symlink = os.path.join(base_dir, db)
        log.debug('Attempting to create symlink {}'.format(symlink))
        # if link creation fails, this is the message that will be displayed.
        message = ('Please create this symlink manually when database is not in use: \n'
                   '\tln -sf {db_dir} {symlink}\n'.format(symlink=symlink, db_dir=db_dir))

        if os.path.islink(symlink):
            # if it is a broken link, remove and re-create
            if not os.path.exists(symlink):
                os.remove(symlink)
                os.symlink(db_dir, symlink)
                log.info('Successfully created symlink from {0} -> {1}'.format(symlink, db_dir))                                
            else:
                # link is valid, check if it is being used 
                try:
                    output = subprocess.check_output(['lsof', '+d', symlink])
                except subprocess.CalledProcessError as e:
                    if not len(e.output):
                        if os.path.exists(symlink):
                            os.remove(symlink)
                        os.symlink(db_dir, symlink)
                        log.info('Successfully created symlink from {0} -> {1}'.format(symlink, db_dir))                
                    else:
                        log.error('Database directory is currently being accessed. {}'.format(message))
                else:
                    log.debug('Output from lsof {}'.format(output))
        else:
            # no link at all, create it
            if not os.path.exists(symlink):
                os.symlink(db_dir, symlink)
                log.info('Successfully created symlink from {0} -> {1}'.format(symlink, db_dir))                
            else:
                log.error('Path exists but is not a symbolic link so leaving '
                          'untouched.\n\t{}'.format(message))

